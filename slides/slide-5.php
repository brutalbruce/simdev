<?php

/* 
 * Copyright (c) 2015, Simple Design <Rob.Xcog at xcogstudios@gmail.com>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * * Redistributions of source code must retain the above copyright notice, this
 *   list of conditions and the following disclaimer.
 * * Redistributions in binary form must reproduce the above copyright notice,
 *   this list of conditions and the following disclaimer in the documentation
 *   and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */


?>
<div id="slide-5" class="slide-5">
<div class="page-header slide-5-header">
    
        
            <div class="slide-5-header">Why working with </div> <img id="slide-1-logo" class="pull-left" fill="white" width="350" height="auto" src="img/sd-svg-white.svg" alt="SimpleDeve Logo"><h1 id="slide-5-h1"><div class="noBreakWhite lobster logo-header" >Simple Development </div><div class="pull-right noBreakWhite>"<small>for your web needs, is </small><strong>Awesome!</strong></div></h1>

    
        </div>
<div class="container">
    
    <div class="row">
    
        
    </div>

    <div class="slide-5-body">
    
    
        <div class="slide-5-section">
            <h2>Able to provide all aspects needed to develop and host websites.</h2>

            <p>
                SimDev can do it all. From concept to production, we can guide you through every step. We can design and manage 
                back-end architecture with the latest in security standards, provide graphics and animations customized to your needs.
                Marketing and UX design are match to the specific nature of each project and our clients needs. Striving to build long relationships 
                with our clients means that their success is our success.
                
            </p>
        </div>
    
        <div class="slide-5-section">
            <h2>Face to Face</h2>
            
            <p>
                Building a custom (anything) is difficult. Building it for others adds ambiguity. Our discovery and outlining process helps
                build an understanding between our clients and us. We have come to find that meeting in person frequently gives us both a better understanding 
                of what is needed and expected. 
                
            </p>
                
                
        
    
        </div>
    
        <div class="slide-5-section">
            <h2>Cost Effective</h2>
            <p>
    We are few yet we are multi-faceted. By providing our array of services under one roof we are able to keep costs to a minimum. <button class="btn btn-primary btn-lg center-block" role="button" data-toggle="modal" data-target="#designNowModal">Get a quote today!</button>

            </p>
        </div>
    
        <div class="slide-5-section">
            <h2> Local Business </h2>
            <p>
                We are proud to contribute to our local Tigard,OR economic family. We hope to strengthen and foster growth in all we serve and work with.
  </p>
        </div>
    </div>
    <div class="slide-5-footer">
    </div>
</div>
        <img width="100%" height="auto" src="/img/einstein-quote.png" alt="a picture of Einstein with the quote">
</div>
