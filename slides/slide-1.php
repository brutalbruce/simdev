<?php
/*
 * Copyright (c) 2015, Simple Design <Rob.Xcog at xcogstudios@gmail.com>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * * Redistributions of source code must retain the above copyright notice, this
 *   list of conditions and the following disclaimer.
 * * Redistributions in binary form must reproduce the above copyright notice,
 *   this list of conditions and the following disclaimer in the documentation
 *   and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
?>
<link rel="stylesheet" type="text/css" href="http://fonts.googleapis.com/css?family=Cinzel|Lobster|Arimo|Fredericka-the-Great|Raleway">

<div id="slide-1">
    <div class="jumbotron slide-1-jt ">
        <div class="container" id="slide-1-container">
            <div class="row">

                <img id="slide-1-logo" class="pull-left" fill="white" width="350" height="auto" src="img/sd-svg-white.svg" alt="SimpleDeve Logo"><h1 id="slide-1-h1"><div class="noBreakWhite pull-left lobster slide-1-header">Simple Development </div><small class="noBreakWhite grey-text ">&nbsp;&nbsp;&nbsp;for all things &#60; web /&#62; </small></h1>
            </div>
            <div class="row">

                <p class="slide-1-header-sub arimo light-text pull-right">Modern and Beautiful Websites Created to Handle Business. <strong> We Got You Covered. </strong>  
                </p> 
            </div>
        </div>
        <br>
        

        <div id="slide-1-sub-header"  <div class="center-block noBreakWhite slide-1-bottom-label"> <span class="glyphicon glyphicon-star" aria-hidden="true"></span> Web <span class="glyphicon glyphicon-star" aria-hidden="true"></span> Mobile <span class="glyphicon glyphicon-star" aria-hidden="true"></span> Graphics <span class="glyphicon glyphicon-star" aria-hidden="true"></span> SEO <span class="glyphicon glyphicon-star" aria-hidden="true"></span> Awesome<span class="glyphicon glyphicon-star" aria-hidden="true"></span></div>

        <img width="100%" height="auto" src="img/devices1920.png" alt="an array of mobile and computer devices">

        <p><button class="btn btn-primary btn-lg center-block" role="button" data-toggle="modal" data-target="#designNowModal">Begin Designing A Website</button></p>
    </div>


</div>

<div class="modal fade" id="designNowModal">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Lets Get Started</h4>
            </div>
            <div class="modal-body">
                <p>Great! Lets figure out what you need in a website. We can do this in person or you can fill out a 
                    digital form and our designers will call you at your earliest convenience.</p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Wait! Go Back!</button>
                <button type="button" target="/frontend/designForm.php" class="btn btn-default">Online Form</button>
                <button type="button" data-dismiss="modal" data-target="/views/modalappointmentReg.php#appointmentModal" data-toggle="modal" class="btn btn-primary">Make Appointment</button>
            </div>

        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

