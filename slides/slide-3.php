<?php
/*
 * Copyright (c) 2015, Simple Design <Rob.Xcog at xcogstudios@gmail.com>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * * Redistributions of source code must retain the above copyright notice, this
 *   list of conditions and the following disclaimer.
 * * Redistributions in binary form must reproduce the above copyright notice,
 *   this list of conditions and the following disclaimer in the documentation
 *   and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
?>
<div id='slide-3' class="slide-3-div">
    <div class="slide-3-top">
        <br>
        <img id="simple-beautiful-img" src="img/simple-beautiful.png" alt="a fancy text of the words Simple Is Beautiful" class="center-block">
        <br>
    </div>
    <div class="jumbotron slide-3-jt">
        <h1 class='slide-3-header  fredrick-the-great'><div class="noBreakWhite">Making Websites Standout &nbsp;&nbsp; </div><small class="noBreakWhite">Depends on strategy and creativity.</small></h1>
        <br>

        <div class="center-block">
            <h2 class="lobster">Getting Your Site Noticed </h2>
        </div>
        <br>
        <div class="raleway light-text slide-3-p"><img width="50px" height="auto" class="pull-left " src="/img/icons/SVG/quotes-left.svg" alt="an icon of quotes">
            Marketing your site to the right people gives you more customers or clients. And that means sales.</div> 
        <div class="raleway light-text slide-3-p"> We make it a point to discover your business and understand it, because as our client, your business success is our success. <img width="50px" height="auto" class=" " src="/img/icons/SVG/quotes-right.svg" alt="an icon of quotes"> </div>

        <button class="btn btn-secondary pull-right btn-lg" id="seo-button" href="frontend/resources/seo.php#" role="button">Learn More About SEO</button>

    </div>

    <div class="slide-3-bottom">


        <div class="container">
            <div class="row">
                <div class="col-sm-6 col-md-4">
                    <div class="thumbnail">
                        <img src="/img/phone-book.jpg" alt="an open phonebook">
                        <div class="caption">
                            <h3 class="slide-3-bottom-h2 center-block fredrick-the-great">Get In Where You Fit In</h3>

                            <p>Search engines aren't the only way to get listed, local and professional listings can help people find your site. Social media can also give your site a boost in popularity.                                
                            <p><a href="#" class="btn btn-primary" role="button">Learn More</a></p>
                        </div>
                    </div>
                </div>

                <div class="col-sm-6 col-md-4">
                    <div class="thumbnail">
                        <img src="/img/game-plan.jpg" alt="background pattern">
                        <div class="caption">
                            <h2 class="slide-3-bottom-h2 center-block fredrick-the-great">
                                Targeted Advertising
                            </h2>

                            <p class="arimo">Creating a market strategy is a powerful tool on the path to success. </p>

                            <p><a href="#" class="btn btn-primary" role="button">Discover More About Marketing</a></p>
                        </div>
                    </div>
                </div>  
                <div class="col-sm-6 col-md-4">
                    <div class="thumbnail">
                        <img src="/img/simpleWeb.png" alt="...">
                        <div class="caption">
                            <h3>It's in the Details</h3>

                            <p>Building concise and descriptive meta-data helps put your website in the places it will be most effective. </p>
                            <p><a href="/frontend/resources/SEO" class="btn btn-primary" role="button">Read More SEO</a> </p>
                        </div>
                    </div>
                </div>

            </div>
        </div>

    </div>
</div>
