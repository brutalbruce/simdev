<?php
/*
 * Copyright (c) 2015, Simple Design <Rob.Xcog at xcogstudios@gmail.com>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * * Redistributions of source code must retain the above copyright notice, this
 *   list of conditions and the following disclaimer.
 * * Redistributions in binary form must reproduce the above copyright notice,
 *   this list of conditions and the following disclaimer in the documentation
 *   and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
?>
<div id='slide-2'>
<div id="slide-2-carousel" class="carousel slide" data-ride="carousel">
    <!-- Indicators -->
    <ol class="carousel-indicators">
        <li data-target="#slide-2-carousel" data-slide-to="0" class="active"></li>
        <li data-target="#slide-2-carousel" data-slide-to="1"></li>
        <li data-target="#slide-2-carousel" data-slide-to="2"></li>
    </ol>

    <!-- Wrapper for slides -->
    <div class="slide-2-carousel-inner carousel-inner" role="listbox">
        <div class="item active">
            <img src="img/couple-at-computer1920.png" alt="couples looking at a computer monitor">
            <div class="carousel-caption">
                <h1 class="slide-2-sheader cinzel light-text">Websites create revenue</h1>
                <p class="arimo slide-2-p">               
                    <button type="button" class="btn btn-primary btn-lg" href='resources/website-revenue.php'>Show Me How</button>   
                </p>
            </div>
        </div>
        <div class="item">
            <img height="100%" width="auto" src="img/customers1920.jpg" alt="a bunch of not smiling smiley faces with one that stands out by smiling">
            <div class="carousel-caption">
                <h1 class="slide-2-sheader text-left  bright-text cinzel ">Web technology can find your perfect customer!</h1>
                <button type="button" class="btn btn-primary btn-lg" href='resources/website-revenue.php'>Show Me How</button>   

            </div>
        </div>  
        <div class="item">
            <img src="img/business-growth1920.jpg" alt="a blurred man drawing a incrementing bar graph on a glass pane infront of him">
            <div class="carousel-caption top-caption">
                <h1 class="slide-2-sheader light-text  cinzel">Websites provide insight</h1>
                <button type="button" class="btn top-btn btn-primary btn-lg" href='resources/website-revenue.php'>Show Me How</button>   

            </div>
        </div>

    </div>


    <!-- Controls -->
    <a class="left carousel-control" href="#slide-2-carousel" role="button" data-slide="prev">
        <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
        <span class="sr-only">Previous</span>
    </a>
    <a class="right carousel-control" href="#slide-2-carousel" role="button" data-slide="next">
        <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
        <span class="sr-only">Next</span>
    </a>
</div>
</div>