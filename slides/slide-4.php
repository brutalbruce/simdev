<?php
/*
 * Copyright (c) 2015, Simple Design <Rob.Xcog at xcogstudios@gmail.com>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * * Redistributions of source code must retain the above copyright notice, this
 *   list of conditions and the following disclaimer.
 * * Redistributions in binary form must reproduce the above copyright notice,
 *   this list of conditions and the following disclaimer in the documentation
 *   and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
?>
<div id="slide-4" class="slide-4-div">
<div class="slide-4-top">
    <div class="logo lobster noBreakWhite pull-right ">
        Simple <img class=" " width="200" height="auto" src="img/sd-svg.svg" alt="SimpleDeve Logo"> Development
    </div>
</div>
<br>

    <!--<div class="container">
    -->
    <div class="row">

        <img width="100%" height="auto" src="/img/chopin1920.png" alt="drawing of Chopin and his quote Simplicity is the highest Goal Acheivable" >
    </div>
    <br>
    <div class="slide-4-body panel panel-default">
        <div class="panel-body">


            <h1 class="slide-4-header raleway">Customer Focused Systems</h1>
            <div class="media">
                <div class="row">
                    <div class="media-body">
                        <p class="arimo">
                            Providing functionality to websites makes them more than just an advertisements. Bridging the space between your clients and your business a website can provide applications in communication, 
                            time management and customer service.
                        </p>

                    </div>
                    <div class="media-right">

                        <img class="img-circle media-object" width="200px" height="auto" src="/img/web-design.jpg" alt="a women with a drawn word, customer over an open hand">


                    </div>

                </div>
            </div>
            <div class="media">
                <div class="row">
                    <div class="media-left">
                        <img class="media-object img-circle" width="200px" height="auto" src="/img/eCommerce.png" alt="a man holding a credit card over a laptop">
                    </div>
                    <div class="media-body">
                        <p>
                            Your own e-commerce site bring your product right to any mobile device or computer.    

                        </p>

                    </div>



                </div>

            </div>

            <div class="media">

                <div class="row">



                    <div class="media-body">
                        <p class="arimo">
                            Forums are a great way for customers to communicate with each other and for your business to respond and learn.
                        </p>

                    </div>
                    <div class="media-right">

                        <img class="img-circle media-object"  width="200px" height="auto" src="/img/forum.png" alt="A screenshot of an internet forum.">


                    </div>
                </div>
            </div>
        </div>

    </div>
</div>
